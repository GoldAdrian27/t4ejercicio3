
public class Ejercicio3App {

	public static void main(String[] args) {

		int x = 7;
		int y = 3;
		
		double n = 5.7;
		double m = 9.7;
		
		//Valor
		System.out.println("Valor de cada variable");
		System.out.println(x);
		System.out.println(y);
		System.out.println(n);
		System.out.println(m);
		
		// X Y
		System.out.println("Operaciones X Y");
		System.out.println("Suma " + (x+y));
		System.out.println("Resta " + (x-y));
		System.out.println("Multiplicacion " + (x*y));
		System.out.println("Division " + (x/y));
		System.out.println("Modulo " + (x%y));
		
		// N M
		System.out.println("Operaciones N M");
		System.out.println("Suma " + (n+m));
		System.out.println("Resta " + (n-m));
		System.out.println("Multiplicacion " + (n*m));
		System.out.println("Division " + (n/m));
		System.out.println("Modulo " + (n%m));
		
		// Extra
		System.out.println("Extra ");
		System.out.println("Suma X + N " + (x+n));
		System.out.println("Division Y M " + (y/m));
		System.out.println("Modulo Y M" + (y%m));
		
		//Doble cada variable
		System.out.println("Doble de cada variable ");
		
		System.out.println("X " + (x*2));
		System.out.println("Y " + (y*2));
		System.out.println("N " + (n*2));
		System.out.println("M " + (m*2));
		
		//suma de todas las variables
		System.out.println("Suma de todas las variables");
		System.out.println(x+y+n+m);
		
		//producto todas las variables
		System.out.println("Multiplicación de todas las variables");
		System.out.println(x+y*n*m);

	}

}
